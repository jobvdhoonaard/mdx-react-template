import Link from 'next/link'
import { TransitionGroup, Transition } from 'react-transition-group'
import tw from 'twin.macro'

function Layout({children, location}) {
    const navigation = [
        {
            name: 'index',
            url: '/'
        },
        {
            name: 'page',
            url: '/page'
        }
    ]

    const TIMEOUT = 200
    const getTransition = {
      entering: {
        position: `absolute`,
        opacity: 0,
        transform: `translateX(50px)`,
      },
      entered: {
        transition: `opacity ${TIMEOUT}ms ease-in-out, transform ${TIMEOUT}ms ease-in-out`,
        opacity: 1,
        transform: `translateX(0px)`,
        animation: "blink .3s linear 2",
      },
      exiting: {
        transition: `opacity ${TIMEOUT}ms ease-in-out, transform ${TIMEOUT}ms ease-in-out`,
        opacity: 0,
        transform: `translateX(-50px)`,
      },
    }

    return [
      <div key="navigation" css={tw`absolute z-50 top-0 w-screen flex flex-row justify-between px-4 py-2`}>
        <span css={tw`font-bold`}>Branding</span>
        <div>
          {navigation.map(item => (
          <Link key={item.name} href={item.url}>
            <a css={tw`ml-2`}>{item.name}</a>
          </Link>
          ))}
        </div>
      </div>,
      <TransitionGroup 
        key="content" 
        style={{ zIndex: 0, position: "relative" }}
      >
        <Transition
          key={location}
          timeout={{
            enter: TIMEOUT,
            exit: TIMEOUT,
          }}
        >
          {status => {
            console.log(status)
            return (
              <div
                style={{
                  ...getTransition[status],
                }}
              >
                {children}
              </div>
          )}}
        </Transition>
      </TransitionGroup>
    ]
  }
  
  export default Layout
  