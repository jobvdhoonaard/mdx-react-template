import { GlobalStyles } from 'twin.macro'
import { useRouter } from 'next/router'
import Layout from '../components/Layout'

const App = ({ Component, pageProps }) => {
  const router = useRouter()
  return (
  <div>
    <GlobalStyles />
    <Layout location={router.pathname}>
      <Component {...pageProps} />
    </Layout>
  </div>
)}

export default App