import tw from 'twin.macro'

function Index() {
  return (
    <div css={tw`flex flex-col w-screen h-screen justify-center items-center`}>
        <div css={tw`text-center`}>
          <span css={tw`text-9xl`}>&#129321;</span>
          <h1 css={tw`text-xl font-bold`}>Yay!</h1>
        </div>
    </div>
  )
}

export default Index